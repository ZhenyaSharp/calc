﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculate
{
    public partial class Form1 : Form
    {
        private double num1, num2;
        private int operation;

        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                return;
            }
            else
            {
                bool result;
                result = double.TryParse(textBox1.Text, out num2);

                if (!result)
                {
                    MessageBox.Show("Ошибка, введите цифры в поле ввода");
                    return;
                }
            }

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text += 0;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text += 1;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text += 2;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text += 3;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text += 4;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text += 5;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text += 6;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text += 7;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox1.Text += 8;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text += 9;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            num1 = double.Parse(textBox1.Text);
            textBox1.Clear();
            operation = 1;
        }

        private void button_Click(object sender, EventArgs e)
        {
            num1 = double.Parse(textBox1.Text);
            textBox1.Clear();
            operation = 2;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            num1 = double.Parse(textBox1.Text);
            textBox1.Clear();
            operation = 3;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            num1 = double.Parse(textBox1.Text);
            textBox1.Clear();
            operation = 4;
        }

        private void Calculate()
        {
            switch (operation)
            {
                case 1:
                    num2 = num1 + double.Parse(textBox1.Text);
                    textBox1.Text = num2.ToString();
                    break;
                case 2:
                    num2 = num1 - double.Parse(textBox1.Text);
                    textBox1.Text = num2.ToString();
                    break;
                case 3:
                    num2 = num1 * double.Parse(textBox1.Text);
                    textBox1.Text = num2.ToString();
                    break;
                case 4:
                    num2 = num1 / double.Parse(textBox1.Text);
                    textBox1.Text = num2.ToString();
                    break;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Calculate();
        }


        private void button16_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (!textBox1.Text.Contains(","))
            {
                textBox1.Text += ",";
            }
        }
    }
}
